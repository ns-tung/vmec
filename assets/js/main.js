$(document).ready(function(){
    
    // Parallax JS: manually refresh the parallax effect
    $(window).trigger('resize').trigger('scroll');
    
    // WOW JS
    new WOW().init(
        {
            boxClass: 'wow',
            animateClass: 'animated',
            offset: 0,
            mobile: true,
            live: true
        }
    );

    // Sticky Navbar
    var $nav = $("#header_main");
        nav_pos = $nav.offset().top;
        nav_height = $nav.height();

    $(document).scroll(function() {
        var scrollTop = $(this).scrollTop();

        if (scrollTop > nav_pos + nav_height) {
            $nav.addClass("fixed-top").animate({top: 0},300);
        } else {
            $nav.removeClass("fixed-top").animate({top:"-50px"}, 0);
        }
    });

    // Tooltip
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })

    // Collapse navbar after link clicked
    $('.navbar-nav').on('click', function(){
        $('.navbar-collapse').collapse('hide');
    });

    // Owl Carousel
    $(".vmec_customers__owl").owlCarousel({
        loop:true,
        nav:true,
        dots:false,
        responsiveClass:true,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:2
            },
            992:{
                items:4
            }
        }
    });
    $(".services_content__owl").owlCarousel({
        loop:true,
        nav:false,
        dots:false,
        margin:30,
        autoplay:true,
        responsiveClass:true,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:2
            },
            992:{
                items:4
            }
        }
    });

    // Go to top
    if ($('#back-to-top').length) {
        var scrollTrigger = 1, // px
        backToTop = function () {
            var scrollTop = $(window).scrollTop();
            if (scrollTop > scrollTrigger) {
                $('#back-to-top').addClass('show');
            } else {
                $('#back-to-top').removeClass('show');
            }
        };
        backToTop();
        $(window).on('scroll', function () {
            backToTop();
        });
        $('#back-to-top').on('click', function (e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: 0
            }, 700);
        });
    }
});